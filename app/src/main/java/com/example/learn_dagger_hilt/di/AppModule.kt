package com.example.learn_dagger_hilt.di

import android.app.Application
import android.util.Log
import com.example.learn_dagger_hilt.data.remote.MyApi
import com.example.learn_dagger_hilt.data.repository.MyRepositoryImpl
import com.example.learn_dagger_hilt.domain.repository.MyRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

const val TAG = "HiltAppModule"

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideMyApi(): MyApi {
        return Retrofit.Builder()
            .baseUrl("https://test.com")
            .build()
            .create(MyApi::class.java)
    }

//    @Provides
//    @Singleton
//    fun provideMyRepository(api: MyApi, context: Application, @Named("hello1") hello1: String): MyRepository {
//        Log.d(TAG, "provideMyRepository: $hello1")
//        return MyRepositoryImpl(api, context)
//    }

    @Provides
    @Singleton
    @Named("hello1")
    fun provideString1() = "String 1"

    @Provides
    @Singleton
    @Named("hello2")
    fun provideString2() = "String 2"


}