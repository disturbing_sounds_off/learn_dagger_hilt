package com.example.learn_dagger_hilt.di

import com.example.learn_dagger_hilt.data.repository.MyRepositoryImpl
import com.example.learn_dagger_hilt.domain.repository.MyRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindMyRepository(
        repositoryImpl: MyRepositoryImpl
    ) : MyRepository
}