package com.example.learn_dagger_hilt.domain.repository

interface MyRepository {
    suspend fun doNetworkCall()
}