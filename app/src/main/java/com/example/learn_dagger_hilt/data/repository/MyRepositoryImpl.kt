package com.example.learn_dagger_hilt.data.repository

import android.app.Application
import android.util.Log
import com.example.learn_dagger_hilt.R
import com.example.learn_dagger_hilt.data.remote.MyApi
import com.example.learn_dagger_hilt.domain.repository.MyRepository
import javax.inject.Inject
import javax.inject.Named


const val TAG = "REPOSITORY_IMPL"
class MyRepositoryImpl @Inject constructor(
    private val api: MyApi,
    private val context: Application,
    @Named("hello1") private val string1: String
) : MyRepository{

   init {
       Log.d(TAG, "printing app name: fuck app context")
       Log.d(TAG, "printing app name: ${context.getString(R.string.app_name)}")
       Log.d(TAG, "printing named string: $string1")
   }

    override suspend fun doNetworkCall() {
        Log.d(TAG, "doNetworkCall: repository was injected successfully ")
    }
}